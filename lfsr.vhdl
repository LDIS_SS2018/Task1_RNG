-------------------------------------------------------------------------------
--
-- Linear-Feedback Shift Register
--
-- GENERICS
--   length: bit width of the lfsr
--
-- INPUTS
--   clk:     clock input
--   run:     '1': run lfsr
--            '0': stop lfsr
--   reset:   '1': reset lfsr to seed
--            '0': normal operation
--   data_in: "data_in xor lfsr(length)" in feedback loop
--   seed:    reset value of the lfsr
--
-- OUTPUTS
--   lfsr_out: output bits of the flip-flops in the lfsr
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity lfsr is
    generic (
        length : integer := 3
    );
    port (
        clk      : in  std_logic;
        run      : in  std_logic                           := '0';
        reset    : in  std_logic                           := '1';
        data_in  : in  std_logic                           := '0';
        seed     : in  std_logic_vector(length-1 downto 0) := (others => '1');
        lfsr_out : out std_logic_vector(length-1 downto 0) := (others => '1')
	);
end lfsr;
--
-------------------------------------------------------------------------------
--
architecture behavioral of lfsr is
    signal lfsr          : std_logic_vector(length downto 1) := seed;
    signal lfsr_feedback : std_logic;

begin
    process (clk, reset, seed)
    begin
        if(reset = '1') then
            lfsr <= seed;
        elsif(clk'event and clk='1') then
            if(run = '1') then
                lfsr <= lfsr(length-1 downto 1) & lfsr_feedback;
            end if;
        end if;
    end process;

    lfsr_out <= lfsr;

    -- https://en.wikipedia.org/wiki/Linear-feedback_shift_register
    lfsr_fb_1 : if length = 1 generate
        lfsr_feedback <= data_in xor lfsr(1);
    end generate lfsr_fb_1;

    lfsr_fb_2 : if length = 2 generate
        lfsr_feedback <= data_in xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_2;

    lfsr_fb_3 : if length = 3 generate
        lfsr_feedback <= data_in xor lfsr(3) xor lfsr(2);
    end generate lfsr_fb_3;
 
    lfsr_fb_4 : if length = 4 generate
        lfsr_feedback <= data_in xor lfsr(4) xor lfsr(3);
    end generate lfsr_fb_4;
    
    lfsr_fb_5 : if length = 5 generate
        lfsr_feedback <= data_in xor lfsr(5) xor lfsr(3);
    end generate lfsr_fb_5;
    
    lfsr_fb_6 : if length = 6 generate
        lfsr_feedback <= data_in xor lfsr(6) xor lfsr(5);
    end generate lfsr_fb_6;
    
    lfsr_fb_7 : if length = 7 generate
        lfsr_feedback <= data_in xor lfsr(7) xor lfsr(6);
    end generate lfsr_fb_7;
    
    lfsr_fb_8 : if length = 8 generate
        lfsr_feedback <= data_in xor lfsr(8) xor lfsr(6) xor lfsr(5) xor lfsr(4);
    end generate lfsr_fb_8;
    
    lfsr_fb_9 : if length = 9 generate
        lfsr_feedback <= data_in xor lfsr(9) xor lfsr(5);
    end generate lfsr_fb_9;
    
    lfsr_fb_10 : if length = 10 generate
        lfsr_feedback <= data_in xor lfsr(10) xor lfsr(7);
    end generate lfsr_fb_10;
    
    lfsr_fb_11 : if length = 11 generate
        lfsr_feedback <= data_in xor lfsr(11) xor lfsr(9);
    end generate lfsr_fb_11;
    
    lfsr_fb_12 : if length = 12 generate
        lfsr_feedback <= data_in xor lfsr(12) xor lfsr(6) xor lfsr(4) xor lfsr(1);
    end generate lfsr_fb_12;
    
    lfsr_fb_13 : if length = 13 generate
        lfsr_feedback <= data_in xor lfsr(13) xor lfsr(4) xor lfsr(3) xor lfsr(1);
    end generate lfsr_fb_13;
    
    lfsr_fb_14 : if length = 14 generate
        lfsr_feedback <= data_in xor lfsr(14) xor lfsr(5) xor lfsr(3) xor lfsr(1);
    end generate lfsr_fb_14;
    
    lfsr_fb_15 : if length = 15 generate
        lfsr_feedback <= data_in xor lfsr(15) xor lfsr(14);
    end generate lfsr_fb_15;
    
    lfsr_fb_16 : if length = 16 generate
        lfsr_feedback <= data_in xor lfsr(16) xor lfsr(15) xor lfsr(13) xor lfsr(4);
    end generate lfsr_fb_16;
    
    lfsr_fb_17 : if length = 17 generate
        lfsr_feedback <= data_in xor lfsr(17) xor lfsr(14);
    end generate lfsr_fb_17;
    
    lfsr_fb_18 : if length = 18 generate
        lfsr_feedback <= data_in xor lfsr(18) xor lfsr(11);
    end generate lfsr_fb_18;
    
    lfsr_fb_19 : if length = 19 generate
        lfsr_feedback <= data_in xor lfsr(19) xor lfsr(6) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_19;
    
    lfsr_fb_20 : if length = 20 generate
        lfsr_feedback <= data_in xor lfsr(20) xor lfsr(17);
    end generate lfsr_fb_20;
    
    lfsr_fb_21 : if length = 21 generate
        lfsr_feedback <= data_in xor lfsr(21) xor lfsr(19);
    end generate lfsr_fb_21;
    
    lfsr_fb_22 : if length = 22 generate
        lfsr_feedback <= data_in xor lfsr(22) xor lfsr(21);
    end generate lfsr_fb_22;
    
    lfsr_fb_23 : if length = 23 generate
        lfsr_feedback <= data_in xor lfsr(23) xor lfsr(18);
    end generate lfsr_fb_23;
    
    lfsr_fb_24 : if length = 24 generate
        lfsr_feedback <= data_in xor lfsr(24) xor lfsr(23) xor lfsr(22) xor lfsr(17);
    end generate lfsr_fb_24;
    
    lfsr_fb_25 : if length = 25 generate
        lfsr_feedback <= data_in xor lfsr(25) xor lfsr(22);
    end generate lfsr_fb_25;
    
    lfsr_fb_26 : if length = 26 generate
        lfsr_feedback <= data_in xor lfsr(26) xor lfsr(6) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_26;
    
    lfsr_fb_27 : if length = 27 generate
        lfsr_feedback <= data_in xor lfsr(27) xor lfsr(5) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_27;
    
    lfsr_fb_28 : if length = 28 generate
        lfsr_feedback <= data_in xor lfsr(28) xor lfsr(25);
    end generate lfsr_fb_28;
    
    lfsr_fb_29 : if length = 29 generate
        lfsr_feedback <= data_in xor lfsr(29) xor lfsr(27);
    end generate lfsr_fb_29;
    
    lfsr_fb_30 : if length = 30 generate
        lfsr_feedback <= data_in xor lfsr(30) xor lfsr(6) xor lfsr(4) xor lfsr(1);
    end generate lfsr_fb_30;
    
    lfsr_fb_31 : if length = 31 generate
        lfsr_feedback <= data_in xor lfsr(31) xor lfsr(28);
    end generate lfsr_fb_31;
    
    lfsr_fb_32 : if length = 32 generate
        lfsr_feedback <= data_in xor lfsr(32) xor lfsr(22) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_32;

    lfsr_fb_33 : if length = 33 generate
        lfsr_feedback <= data_in xor lfsr(33) xor lfsr(20);
    end generate lfsr_fb_33;

    lfsr_fb_34 : if length = 34 generate
        lfsr_feedback <= data_in xor lfsr(34) xor lfsr(27) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_34;

    lfsr_fb_35 : if length = 35 generate
        lfsr_feedback <= data_in xor lfsr(35) xor lfsr(33);
    end generate lfsr_fb_35;

    lfsr_fb_36 : if length = 36 generate
        lfsr_feedback <= data_in xor lfsr(36) xor lfsr(25);
    end generate lfsr_fb_36;

    lfsr_fb_37 : if length = 37 generate
        lfsr_feedback <= data_in xor lfsr(37) xor lfsr(5) xor lfsr(4) xor lfsr(3) xor lfsr(2) xor lfsr(1);
    end generate lfsr_fb_37;

    lfsr_fb_38 : if length = 38 generate
        lfsr_feedback <= data_in xor lfsr(38) xor lfsr(6) xor lfsr(5) xor lfsr(1);
    end generate lfsr_fb_38;

    lfsr_fb_39 : if length = 39 generate
        lfsr_feedback <= data_in xor lfsr(39) xor lfsr(35);
    end generate lfsr_fb_39;

    lfsr_fb_40 : if length = 40 generate
        lfsr_feedback <= data_in xor lfsr(40) xor lfsr(38) xor lfsr(21) xor lfsr(19);
    end generate lfsr_fb_40;

    lfsr_fb_41 : if length = 41 generate
        lfsr_feedback <= data_in xor lfsr(41) xor lfsr(38);
    end generate lfsr_fb_41;

    lfsr_fb_42 : if length = 42 generate
        lfsr_feedback <= data_in xor lfsr(42) xor lfsr(41) xor lfsr(20) xor lfsr(19);
    end generate lfsr_fb_42;

    lfsr_fb_43 : if length = 43 generate
        lfsr_feedback <= data_in xor lfsr(43) xor lfsr(42) xor lfsr(38) xor lfsr(37);
    end generate lfsr_fb_43;

    lfsr_fb_44 : if length = 44 generate
        lfsr_feedback <= data_in xor lfsr(44) xor lfsr(43) xor lfsr(18) xor lfsr(17);
    end generate lfsr_fb_44;

    lfsr_fb_45 : if length = 45 generate
        lfsr_feedback <= data_in xor lfsr(45) xor lfsr(44) xor lfsr(42) xor lfsr(41);
    end generate lfsr_fb_45;

    lfsr_fb_46 : if length = 46 generate
        lfsr_feedback <= data_in xor lfsr(46) xor lfsr(45) xor lfsr(26) xor lfsr(25);
    end generate lfsr_fb_46;

    lfsr_fb_47 : if length = 47 generate
        lfsr_feedback <= data_in xor lfsr(47) xor lfsr(42);
    end generate lfsr_fb_47;

    lfsr_fb_48 : if length = 48 generate
        lfsr_feedback <= data_in xor lfsr(48) xor lfsr(47) xor lfsr(21) xor lfsr(20);
    end generate lfsr_fb_48;

    lfsr_fb_49 : if length = 49 generate
        lfsr_feedback <= data_in xor lfsr(49) xor lfsr(40);
    end generate lfsr_fb_49;

    lfsr_fb_50 : if length = 50 generate
        lfsr_feedback <= data_in xor lfsr(50) xor lfsr(49) xor lfsr(24) xor lfsr(23);
    end generate lfsr_fb_50;

    lfsr_fb_51 : if length = 51 generate
        lfsr_feedback <= data_in xor lfsr(51) xor lfsr(50) xor lfsr(36) xor lfsr(35);
    end generate lfsr_fb_51;

    lfsr_fb_52 : if length = 52 generate
        lfsr_feedback <= data_in xor lfsr(52) xor lfsr(49);
    end generate lfsr_fb_52;

    lfsr_fb_53 : if length = 53 generate
        lfsr_feedback <= data_in xor lfsr(53) xor lfsr(52) xor lfsr(38) xor lfsr(37);
    end generate lfsr_fb_53;

    lfsr_fb_54 : if length = 54 generate
        lfsr_feedback <= data_in xor lfsr(54) xor lfsr(53) xor lfsr(18) xor lfsr(17);
    end generate lfsr_fb_54;

    lfsr_fb_55 : if length = 55 generate
        lfsr_feedback <= data_in xor lfsr(55) xor lfsr(31);
    end generate lfsr_fb_55;

    lfsr_fb_56 : if length = 56 generate
        lfsr_feedback <= data_in xor lfsr(56) xor lfsr(55) xor lfsr(35) xor lfsr(34);
    end generate lfsr_fb_56;

    lfsr_fb_57 : if length = 57 generate
        lfsr_feedback <= data_in xor lfsr(57) xor lfsr(50);
    end generate lfsr_fb_57;

    lfsr_fb_58 : if length = 58 generate
        lfsr_feedback <= data_in xor lfsr(58) xor lfsr(39);
    end generate lfsr_fb_58;

    lfsr_fb_59 : if length = 59 generate
        lfsr_feedback <= data_in xor lfsr(59) xor lfsr(58) xor lfsr(38) xor lfsr(37);
    end generate lfsr_fb_59;

    lfsr_fb_60 : if length = 60 generate
        lfsr_feedback <= data_in xor lfsr(60) xor lfsr(59);
    end generate lfsr_fb_60;

    lfsr_fb_61 : if length = 61 generate
        lfsr_feedback <= data_in xor lfsr(61) xor lfsr(60) xor lfsr(46) xor lfsr(45);
    end generate lfsr_fb_61;

    lfsr_fb_62 : if length = 62 generate
        lfsr_feedback <= data_in xor lfsr(62) xor lfsr(61) xor lfsr(6) xor lfsr(5);
    end generate lfsr_fb_62;

    lfsr_fb_63 : if length = 63 generate
        lfsr_feedback <= data_in xor lfsr(63) xor lfsr(62);
    end generate lfsr_fb_63;

    lfsr_fb_64 : if length = 64 generate
        lfsr_feedback <= data_in xor lfsr(64) xor lfsr(63) xor lfsr(61) xor lfsr(60);
    end generate lfsr_fb_64;

    lfsr_fb_65 : if length = 65 generate
        lfsr_feedback <= data_in xor lfsr(65) xor lfsr(47);
    end generate lfsr_fb_65;

    lfsr_fb_66 : if length = 66 generate
        lfsr_feedback <= data_in xor lfsr(66) xor lfsr(65) xor lfsr(57) xor lfsr(56);
    end generate lfsr_fb_66;

    lfsr_fb_67 : if length = 67 generate
        lfsr_feedback <= data_in xor lfsr(67) xor lfsr(66) xor lfsr(58) xor lfsr(57);
    end generate lfsr_fb_67;

    lfsr_fb_68 : if length = 68 generate
        lfsr_feedback <= data_in xor lfsr(68) xor lfsr(59);
    end generate lfsr_fb_68;

    -- else path for length > 68 
    lfsr_fb_else : if length > 68 generate
        lfsr_feedback <= data_in xor lfsr(68) xor lfsr(59);
    end generate lfsr_fb_else;

end behavioral;
--
-------------------------------------------------------------------------------
