-------------------------------------------------------------------------------
--
-- handles hardware input signals (debounce, edge detection)
--
-- GENERICS
--   WAIT_TICKS:   delay cicles in clock ticks
--   ACTIVE_STATE: button signal in pressed state
--
-- INPUTS
--   clk:       clock input
--   reset:     '1': reset lfsr to seed
--              '0': normal operation
--   signal_in: input signal
--
-- OUTPUTS
--   db_signal_out: db_signal output signal
--   pos_edge_out:  edge from inactive state to active state
--   neg_edge_out:  edge from active state to inactive state
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity hw_signal_handler is
    generic (
		WAIT_TICKS   : integer := 1e4; -- 0.1ms for 100MHz clock
        ACTIVE_STATE : bit     := '1'
	);
    port (
		clk           : in std_logic;
        reset         : in std_logic := '1';
		signal_in     : in std_logic;
        db_signal_out : out std_logic := '0';
        pos_edge_out  : out std_logic := '0';
        neg_edge_out  : out std_logic := '0'
	);
end hw_signal_handler;
--
-------------------------------------------------------------------------------
--
architecture behavioral of hw_signal_handler is
    -- input signal active high
    signal signal_in_ah  : std_logic                     := '0'; 
    signal db_input_ff   : std_logic_vector(1 downto 0)  := (others => '0');
    signal edge_input_ff : std_logic_vector(1 downto 0)  := (others => '0');
    signal count         : integer range 0 to WAIT_TICKS :=  0;
    signal db_signal     : std_logic;

begin
    with ACTIVE_STATE select signal_in_ah <= 
        signal_in     when '1',
        not signal_in when '0';

    debounce : process(clk)
    begin
        if(clk'event and clk = '1') then
            db_input_ff <= db_input_ff(0) & signal_in_ah;

            if(reset = '1') then
                db_input_ff  <= (others => '0');
                count     <= 0;
                db_signal <= '0';
            elsif(db_input_ff(0) /= db_input_ff(1)) then
                count <= 0;
            elsif(count < WAIT_TICKS) then
                count <= count + 1;
            else 
                db_signal <= db_input_ff(1);
            end if;
        end if;
    end process debounce;

    db_signal_out <= db_signal;

    edge : process(clk)
    begin
        if(clk'event and clk = '1') then 
            pos_edge_out  <= '0';
            neg_edge_out  <= '0';
            edge_input_ff <= edge_input_ff(0) & db_signal;

            if(reset = '1') then
                edge_input_ff <= (others => '0');
            elsif(edge_input_ff(0) = '1' and edge_input_ff(1) = '0') then
                pos_edge_out <= '1';
            elsif(edge_input_ff(0) = '0' and edge_input_ff(1) = '1') then
                neg_edge_out <= '1';
            end if;
        end if;   
    end process edge;
end behavioral;