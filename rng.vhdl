-------------------------------------------------------------------------------
--
-- Random Number Generator
--
-- GENERICS
--   length:     bit width of the random number
--   ro_polynom: generator polynom of the Ring Oscillator
--
-- INPUTS
--   clk:   clock input
--   reset: '1': reset lfsr to seed
--          '0': normal operation
--   seed:  reset value of the lfsr
--
-- OUTPUTS
--   random_number: random number output
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity rng is
    generic (
        length     : integer          := 3;
        ro_polynom : std_logic_vector := "100011101"
    );
    port (
		clk           : in  std_logic;
        reset         : in  std_logic := '1';
        seed          : in  std_logic_vector(length-1 downto 0) := (others => '1');
        random_number : out std_logic_vector(length-1 downto 0) := (others => '0')
	);
end rng;
--
-------------------------------------------------------------------------------
--
architecture behavioral of rng is
    signal   rbg_out  : std_logic := '0';
    signal   run      : std_logic := '0';
    signal   counter  : integer   := 0;

    component rbg is
        generic (
            ro_type : integer;
            polynom : std_logic_vector
        );
        port (
            clk     : in  std_logic;
            reset   : in  std_logic;
            rbg_out : out std_logic
        );
    end component;

    component lfsr is
        generic (
            length : integer := 3
        );
        port (
            clk      : in  std_logic;
            run      : in  std_logic;
            reset    : in  std_logic;
            data_in  : in  std_logic;
            seed     : in  std_logic_vector(length-1 downto 0);
            lfsr_out : out std_logic_vector(length-1 downto 0)
        );
    end component;

begin

    -- Enable Period Timer
    process (clk, reset)
    begin
        if(clk'event and clk = '1') then 
            if(reset='0' and counter < 1000) then
                counter <= counter + 1;
            else
                counter <= 0;
            end if;
        end if;
    end process;

    -- Enable Ring Oscillator Controll Circuit
    process (clk, reset, counter)
    begin
        if(reset='0' and counter < 100) then
            run <= '1';
        else
            run <= '0';
        end if;
    end process;

    -- Random Bit Generator
    rbg_inst: rbg 
        generic map(0, ro_polynom)
        port map(clk, "not" (run), rbg_out);

    -- Postprocessung Unit (LFSR)
    lfsr_inst: lfsr 
        generic map(length)
        port map(clk, run, reset, rbg_out, seed, random_number);

end behavioral;
--
-------------------------------------------------------------------------------
