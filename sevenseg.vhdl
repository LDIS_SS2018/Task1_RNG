-------------------------------------------------------------------------------
--
-- 7-segment display
-- 
-- GENERICS
--   length: bit width of the number to display
--
-- INPUTS
--   clk:        clock input
--   set:        1 for one cycle to save values from input port
--   number_hex: data to be displayed
--
-- OUTPUTS
--   cathodes: eight cathodes of the 7-Segment display
--   anodes:   used annodes of the 7-Segment display
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity sevenseg is
	generic (
		length : integer := 16
	);
	port (
		clk			: in  std_logic;
		set         : in  std_logic;
        number_hex  : in  std_logic_vector(length-1 downto 0);		
		cathodes	: out std_logic_vector(7 downto 0);
        anodes      : out std_logic_vector((length-1)/4 downto 0)
	);
end sevenseg;
--
-------------------------------------------------------------------------------
--
architecture behavioral of sevenseg is
	signal current_annode : integer                                    := 0;
	signal CA_cur : std_logic_vector(3 downto 0)                       := "0000";
	signal CA_buffer : std_logic_vector(((length-1)/4+1)*4-1 downto 0) := (others => '0'); -- round up to 4 bit (one digit)
	signal shift_count : integer                                       := 0;

begin
	process (clk)
	begin
		if (clk'event and clk='1') then
			-- slow shifting between different blocks down --
			if (shift_count >= 100e3) then  -- for input clock 100e6 -> 100e3
				shift_count <= 0;
				-- seven blocks
				if (current_annode >= (length-1)/4) then 
					current_annode <= 0;
				else
					current_annode <= current_annode + 1; -- select next element
				end if;
			else
				shift_count <= shift_count + 1;
			end if;

			-- buffer input values
            if (set = '1') then 
                	CA_buffer(length-1 downto 0) <= number_hex;	
            end if;

			-- select current device kathodes for selected anode
			CA_cur <= CA_buffer((current_annode+1)*4-1 downto (current_annode)*4);
			case  CA_cur is
				-- CA.. seg(7) - DP ..seg(0)
				--------------------------abcdefg----------
				when "0000"=> cathodes <="00000011";  -- '0'
				when "0001"=> cathodes <="10011111";  -- '1'
				when "0010"=> cathodes <="00100101";  -- '2'
				when "0011"=> cathodes <="00001101";  -- '3'
				when "0100"=> cathodes <="10011001";  -- '4'
				when "0101"=> cathodes <="01001001";  -- '5'
				when "0110"=> cathodes <="01000001";  -- '6'
				when "0111"=> cathodes <="00011111";  -- '7'
				when "1000"=> cathodes <="00000001";  -- '8'
				when "1001"=> cathodes <="00001001";  -- '9'
                when "1010"=> cathodes <="00010000";  -- 'A' 
				when "1011"=> cathodes <="11000000";  -- 'B'
                when "1100"=> cathodes <="01100010";  -- 'C'
                when "1101"=> cathodes <="10000100";  -- 'D'
                when "1110"=> cathodes <="01100000";  -- 'E'
                when "1111"=> cathodes <="01110000";  -- 'F'
				when others=> cathodes <="11111111";

			end case;
			
			-- next anode (next block)
			anodes <= (others => '1');			-- Anode , low active
            anodes(current_annode) <= '0';
		end if;
--
-------------------------------------------------------------------------------
--
	end process;

end behavioral;
--
-------------------------------------------------------------------------------
