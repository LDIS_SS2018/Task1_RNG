-------------------------------------------------------------------------------
--
-- Galois Ring Oscillator
--
-- GENERICS
--   polynom: generator polynom in descending order (e.g. "100011101" = x^8 + x^4 + x^3 + x^2 + 1)
--
-- INPUTS
--   reset:  '0': enable garo
--           '1': reset garo to zeros
--
-- OUTPUTS
--   garo_out: output of the last inverter
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity garo is
    generic (
        polynom : std_logic_vector := "100011101"
    );
    port (
        reset    : in  std_logic := '1';
        garo_out : out std_logic := '0'
	);
end garo;
--
-------------------------------------------------------------------------------
--
architecture behavioral of garo is
    attribute KEEP : string; -- prevent optimization while synthesis
    attribute S    : string; -- prevent optimization while implementation
        signal garo : std_logic_vector(polynom'length-1 downto 0) := (others => '0');
    attribute KEEP of garo: signal is "True";
    attribute S    of garo: signal is "True";

begin
    garo_circuit : for i in 1 to polynom'length-2 generate
        inv: if polynom(i) = '0' generate 
            garo(i) <= (not garo(i-1)) and not reset after 13 ps;
        end generate inv;

        inv_xor: if polynom(i) = '1' generate 
            garo(i) <= ((not garo(i-1)) xor garo(polynom'length-1)) and not reset after 13 ps;
        end generate inv_xor;
    end generate;

    garo(0)                 <= garo(polynom'length-1);
    garo(polynom'length-1)  <= not garo(polynom'length-2);
    garo_out                <= garo(polynom'length-1);
end behavioral;
--
-------------------------------------------------------------------------------
