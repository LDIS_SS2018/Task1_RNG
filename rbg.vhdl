-------------------------------------------------------------------------------
--
-- Random Bit Generator
--
-- GENERICS
--   ro_type: type of Ring Oscillator
--              0: GARO
--              1: FIRO (not implemented)
--              2: GARO xor FIRO (not implemented)
--   polynom: generator polynom in descending order
--
-- INPUTS
--   clk:    clock input
--   reset:  '0': enable rbg
--           '1': reset rbg to zeros
--
-- OUTPUTS
--   rbg_out: output of the last inverter
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity rbg is
    generic (
        ro_type : integer := 0;
        polynom : std_logic_vector := "100011101"
    );
    port (
        clk     : in  std_logic;
        reset   : in  std_logic := '1';
        rbg_out : out std_logic := '0'
	);
end rbg;
--
-------------------------------------------------------------------------------
--
architecture behavioral of rbg is
    signal garo_out : std_logic := '0';
    signal toggle_ff_out : std_logic := '0';
    signal sample_ff_out : std_logic := '0';

    component garo
        generic (
            polynom : std_logic_vector
        );
        port (
            reset    : in  std_logic;
            garo_out : out std_logic
        );
    end component;

begin
    -- Galois Ring Oscillator
    garo_inst : if ro_type = 0 generate
         ro : garo 
            generic map(polynom)
            port map(reset, garo_out);
    end generate;

    -- Toggle Flip Flop
    toggle_ff: process (garo_out, reset)
    begin
        if(reset = '1') then
            toggle_ff_out <= '0';
        elsif(garo_out'event and garo_out='1') then
            toggle_ff_out <= not toggle_ff_out;
        end if;
    end process toggle_ff;

    -- Sample Flip Flop
    sample_ff: process (clk, reset)
    begin
        if(reset = '1') then
            sample_ff_out <= '0';
        elsif(clk'event and clk='1') then
            sample_ff_out <= toggle_ff_out;
        end if;
    end process sample_ff;

    rbg_out <= sample_ff_out;
end behavioral;
--
-------------------------------------------------------------------------------
