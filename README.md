To automate the simulation, synth and prog scripts, the VHDL and Constraint files has to have the same name as the entity.

The entity name is commited as argument:

    vivado -mode batch -source synth.tcl -tclargs {entity_name}
    
    vivado -mode batch -source prog.tcl -tclargs {entity_name}

