-------------------------------------------------------------------------------
--
-- Application for RNG
--
-- GENERICS
--   length:     bit width of the random number
--   ro_polynom: generator polynom of the Ring Oscillator
--
-- INPUTS
--   CLK: clock input
--
--   BTNC: center button
--   BTNR: right button
--   SW:   switch 0
--
--   UART_RECEIVE: UART Rx input
--
-- OUTPUTS
--   LED:
--
--   SEVENSEG_CATH: eight cathodes of the 7-Segment display
--   SEVENSEG_AN:   eight annodes of the 7-Segment display
--
--   UART_TRANSMIT: UART Tx output
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
--
-------------------------------------------------------------------------------
--
entity application is
    generic (
        -- length of random number in bits
        length     : integer          := 64;
        -- polynom for random bit generator
        ro_polynom : std_logic_vector := "100011101"
    );
    port (
		CLK      : in std_logic;
		
        -- Inputs
        BTNC : in std_logic;
        BTNR : in std_logic;
        SW   : in std_logic_vector(0 downto 0);

        -- LEDs
        LED     : out std_logic_vector(15 downto 0) := (others => '0');
        LED17_R : out std_logic;

        -- 7-Segment
        SEVENSEG_CATH : out std_logic_vector(7 downto 0);
        SEVENSEG_AN   : out std_logic_vector(7 downto 0);
        
        -- UART
        UART_RECEIVE  : in std_logic;
		UART_TRANSMIT : out std_logic
	);
end application;
--
-------------------------------------------------------------------------------
--
architecture behavioral of application is
	constant CLK_FREQ    	  : integer := 100E6;
	constant BAUDRATE         : integer := 500000;
    -- count of 128 bit numbers to send in test mode
    constant TM_NUMBERS_COUNT : integer := 1e5; 

    -- Random Number Generator
    signal rng_seed                  : 
        std_logic_vector(length-1 downto 0) := (others => '1');
    signal rng_seed_next             : 
        std_logic_vector(length-1 downto 0) := (others => '1');
    signal random_number             : 
        std_logic_vector(length-1 downto 0) := rng_seed;
    signal random_number_buffer      : 
        std_logic_vector(((length-1)/8+1)*8-1 downto 0);
    signal random_number_buffer_next : 
        std_logic_vector(((length-1)/8+1)*8-1 downto 0);

    -- 7-Segment
    type switch_t is array (boolean) of integer;
    constant switch          : switch_t := (true => length, false => 32);
    constant sevenseg_length : integer  := switch(length < 32); 

    signal sevenseg_number_hex      : 
        std_logic_vector(sevenseg_length-1 downto 0) := 
            rng_seed(sevenseg_length-1 downto 0);
    signal sevenseg_number_hex_next : 
        std_logic_vector(sevenseg_length-1 downto 0) := 
            rng_seed(sevenseg_length-1 downto 0);

    -- UART Rx
    signal uart_rx_data                  : std_logic_vector(7 downto 0);
    signal uart_rx_data_buffer           : 
        std_logic_vector(((length-1)/8+1)*8-1 downto 0);
    signal uart_rx_data_buffer_next      : 
        std_logic_vector(((length-1)/8+1)*8-1 downto 0);
    signal uart_rx_bytes_to_receive      : integer := (length-1)/8+1;
    signal uart_rx_bytes_to_receive_next : integer := (length-1)/8+1;
	signal uart_rx_new_data              : std_logic;

    -- UART Tx
    signal uart_tx_data   	          : std_logic_vector(7 downto 0);
    signal uart_tx_data_next   	      : std_logic_vector(7 downto 0);
    signal uart_tx_send   	          : std_logic;
    signal uart_tx_send_next   	      : std_logic;
    signal uart_tx_ready              : std_logic;
	signal uart_tx_bytes_to_send      : integer := 0;
	signal uart_tx_bytes_to_send_next : integer := 0;

    -- debounced input signals
    signal reset  : std_logic := '1'; -- BTNC
    signal new_rn : std_logic := '0'; -- BTNR pos edge
    signal mode   : std_logic := '0'; -- SW0; 0: Productive mode; 1: Test mode

    -- Test Mode
    signal tm_progress	    : std_logic_vector(15 downto 0) := (others => '0');
	signal tm_progress_next : std_logic_vector(15 downto 0) := (others => '0');
    signal tm_count         : integer                       := 0;
	signal tm_count_next    : integer                       := 0;

    -- Mode State Machine
    type mode_state_t is (
		STATE_IDLE,
        STATE_TX,
		STATE_TEST_MODE, 
		STATE_STILL_TO_SEND
	);
    signal mode_state      : mode_state_t := STATE_IDLE;
    signal mode_state_next : mode_state_t := STATE_IDLE;

    -- UART Receive State Machine
    type rx_state_t is (
        STATE_IDLE,
        STATE_READ_DATA,
        STATE_WAIT,
        STATE_SAVE
    );
    signal rx_state      : rx_state_t := STATE_IDLE;
    signal rx_state_next : rx_state_t := STATE_IDLE;

    -- Component declarations
    component sevenseg is
        generic (
            length : integer
        );
        port (
            clk			: in  std_logic;
            set         : in  std_logic;
            number_hex  : in  std_logic_vector(length-1 downto 0);		
            cathodes	: out std_logic_vector(7 downto 0);
            anodes      : out std_logic_vector((length-1)/4 downto 0)
        );
    end component;

    component uart_rx is
    	generic (
            CLK_FREQ : integer; -- in Hz
            BAUDRATE : integer  -- in bit/s
	    );
        port (
            clk      : in std_logic;
            rst      : in std_logic;
            rx       : in std_logic;
            data     : out std_logic_vector(7 downto 0);
            data_new : out std_logic
        );
    end component;

    component uart_tx is
    	generic (
            CLK_FREQ : integer; -- in Hz
            BAUDRATE : integer  -- in bit/s
        );
        port (
            clk   : in std_logic;
            rst   : in std_logic;
            send  : in std_logic;
            data  : in std_logic_vector(7 downto 0);
            rdy   : out std_logic;
            tx    : out std_logic
        );
    end component;

    component rng is
        generic (
            length     : integer;
            ro_polynom : std_logic_vector
        );
        port (
            clk           : in  std_logic;
            reset         : in  std_logic;
            seed          : in  std_logic_vector(length-1 downto 0);
            random_number : out std_logic_vector(length-1 downto 0)
        );
    end component;

    component hw_signal_handler is
        generic (
            WAIT_TICKS   : integer;
            ACTIVE_STATE : bit
        );
        port (
            clk           : in std_logic;
            reset         : in std_logic;
            signal_in     : in std_logic;
            db_signal_out : out std_logic;
            pos_edge_out  : out std_logic;
            neg_edge_out  : out std_logic
        );
    end component;
begin
--
-------------------------------------------------------------------------------
--
-- Component Instantiation
--
    -- Sevensegment Display
    sevenseg_inst: sevenseg
        generic map(sevenseg_length)
        port map(CLK, '1', sevenseg_number_hex, SEVENSEG_CATH,
         SEVENSEG_AN((sevenseg_length-1)/4 downto 0));

    -- UART Rx
    uart_rx_inst: uart_rx
        generic map(CLK_FREQ, BAUDRATE)
        port map(CLK, reset, UART_RECEIVE, uart_rx_data, uart_rx_new_data);

    -- UART Tx
    uart_tx_inst: uart_tx
        generic map(CLK_FREQ, BAUDRATE)
        port map(CLK, reset, uart_tx_send, 
                uart_tx_data, uart_tx_ready, UART_TRANSMIT);
    
    -- Random Number Generator
    rng_inst: rng
        generic map(length, ro_polynom)
        port map(CLK, reset, rng_seed, random_number);

    -- read BTNC (reset)
    hw_signal_handler_btnc_inst: hw_signal_handler
        generic map(1e4, '1')   --normal use: 1e4; simulation 1e1 ... debounce
        port map(CLK, '0', BTNC, reset, open, open);

    -- read BTNC (new_rn)
    hw_signal_handler_btnr_inst: hw_signal_handler
        generic map(1e4, '1')
        port map(CLK, reset, BTNR, open, new_rn, open);

    -- read SW(0) (mode)
    hw_signal_handler_sw0_inst: hw_signal_handler
        generic map(1e4, '1')
        port map(CLK, reset, SW(0), mode, open, open);

--
-------------------------------------------------------------------------------
--
-- State Machine to handle Test- and Productive mode
--
	mode_sync : process (CLK, reset)
	begin

		if(reset = '1') then 
            -- reset all variables to intitial state
            random_number_buffer                    <= (others => '0');
            random_number_buffer(length-1 downto 0) <= rng_seed;

			uart_tx_send 	      <= '0';
			uart_tx_data 	      <= (others => '0');
			uart_tx_bytes_to_send <= 0;

			sevenseg_number_hex <= rng_seed(sevenseg_length-1 downto 0);

			tm_count    <= 0;
            tm_progress <= (others => '0');

            mode_state <= STATE_IDLE;

		elsif(rising_edge(CLK)) then
            -- transfer variables 
            random_number_buffer <= random_number_buffer_next;

			uart_tx_send          <= uart_tx_send_next;
			uart_tx_data          <= uart_tx_data_next;
            uart_tx_bytes_to_send <= uart_tx_bytes_to_send_next;

			sevenseg_number_hex <= sevenseg_number_hex_next;

			tm_count    <= tm_count_next;
            tm_progress <= tm_progress_next;

            mode_state <= mode_state_next;
		end if;
	end process mode_sync;

    mode_state_machine : process(CLK)
	begin
		-- prevent latches
        random_number_buffer_next  <= random_number_buffer;
		uart_tx_send_next          <= uart_tx_send;
		uart_tx_data_next          <= uart_tx_data;
		uart_tx_bytes_to_send_next <= uart_tx_bytes_to_send;
		sevenseg_number_hex_next   <= sevenseg_number_hex;
		tm_count_next              <= tm_count;
        tm_progress_next           <= tm_progress;
        mode_state_next            <= mode_state;

		case mode_state is
				when STATE_IDLE =>
                    ----------- waiting for user input -----------
					if(mode = '1' and new_rn = '1') then -- Test mode
						tm_count_next   <= TM_NUMBERS_COUNT * 16 / ((length-1)/8+1);
						mode_state_next <= STATE_TEST_MODE;
                    elsif(mode = '0' and new_rn = '1') then -- Productive mode
                        random_number_buffer_next                    <= (others => '0');
						random_number_buffer_next(length-1 downto 0) <= random_number;
                        uart_tx_bytes_to_send_next                   <= (length-1)/8+1;
                        mode_state_next                              <= STATE_TX;
					else
                        uart_tx_send_next <= '0';
                        tm_progress_next  <= (others => '0');
                        mode_state_next   <= STATE_IDLE;
					end if;

				when STATE_TEST_MODE => 
                    ----------- Testing mode -----------
                    -- produce <TM_NUMBERS_COUNT> 128bit random 
                    -- numbers and send them via uart
					if(mode = '1' and tm_count > 0) then
                        random_number_buffer_next                    <= (others => '0');
						random_number_buffer_next(length-1 downto 0) <= random_number;
                        uart_tx_bytes_to_send_next                   <= (length-1)/8+1; 
						tm_count_next                                <= tm_count - 1;
                        mode_state_next                              <= STATE_TX; 

						-- display progress
						if    tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 15/16 then 
							tm_progress_next <= x"FFFF";
                        elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 14/16 then
							tm_progress_next <= x"7FFF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 13/16 then 
							tm_progress_next <= x"3FFF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 12/16 then 
							tm_progress_next <= x"1FFF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 11/16 then 
							tm_progress_next <= x"0FFF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 10/16 then 
							tm_progress_next <= x"07FF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 9/16 then 
							tm_progress_next <= x"03FF";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 8/16 then 
							tm_progress_next <= x"01FF";
                        elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 7/16 then 
							tm_progress_next <= x"00FF";
                        elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 6/16 then
							tm_progress_next <= x"007F";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 5/16 then 
							tm_progress_next <= x"003F";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 4/16 then 
							tm_progress_next <= x"001F";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 3/16 then 
							tm_progress_next <= x"000F";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 2/16 then 
							tm_progress_next <= x"0007";
						elsif tm_count > TM_NUMBERS_COUNT * 16 / ((length-1)/8+1) * 1/16 then 
							tm_progress_next <= x"0003";
                        elsif tm_count > 1 then
                            tm_progress_next <= x"0001";
						else
							tm_progress_next <= x"0000";
						end if;
					else
						mode_state_next <= STATE_IDLE;
					end if;	
				
				when STATE_TX =>
                    ----------- transmit data via uart -----------
					uart_tx_send_next <= '0';


                    sevenseg_number_hex_next <= random_number_buffer(sevenseg_length-1 downto 0);

					if(mode = '0') then -- productive mode
						if(uart_tx_bytes_to_send > 0) then
							mode_state_next <= STATE_STILL_TO_SEND;	
						else
							mode_state_next <= STATE_IDLE;
						end if;
					elsif(mode = '1') then  -- testing mode
						if(uart_tx_bytes_to_send > 0) then
							mode_state_next <= STATE_STILL_TO_SEND;
						else
							mode_state_next <= STATE_TEST_MODE;
						end if;
					else
						mode_state_next <= STATE_IDLE;
					end if;

                -- keep on sending
				when STATE_STILL_TO_SEND =>
                ----------- send a full random number -----------
					if(uart_tx_ready = '1') then
						uart_tx_send_next          <= '1';
						uart_tx_data_next          <= random_number_buffer(8*(uart_tx_bytes_to_send)-1 downto 8*(uart_tx_bytes_to_send-1));
						uart_tx_bytes_to_send_next <= uart_tx_bytes_to_send - 1;
						mode_state_next            <= STATE_TX;
					else 
                    -- loop until writeable
						mode_state_next <= STATE_STILL_TO_SEND;
					end if;
			end case;
	end process mode_state_machine;
	
    -- LED control
    LED     <= tm_progress;
    LED17_R <= reset;

--
-------------------------------------------------------------------------------
--
-- State Machine to write UART Rx data to rng_seed
--
    rx_sync : process (CLK, reset)
    begin
        if(reset = '1') then
            rx_state                 <= STATE_IDLE;
            uart_rx_data_buffer      <= (others => '0');
            uart_rx_bytes_to_receive <= 0;
        elsif(CLK'event and CLK='1') then   
            rx_state                 <= rx_state_next;
            uart_rx_data_buffer      <= uart_rx_data_buffer_next;
            uart_rx_bytes_to_receive <= uart_rx_bytes_to_receive_next;
            rng_seed                 <= rng_seed_next;
        end if;
    end process rx_sync;

    rx_state_machine : process (CLK)
    begin
        rx_state_next                 <= rx_state;
        uart_rx_data_buffer_next      <= uart_rx_data_buffer;
        uart_rx_bytes_to_receive_next <= uart_rx_bytes_to_receive;
        rng_seed_next                 <= rng_seed;

        case rx_state is
            when STATE_IDLE => 
                if(uart_rx_new_data = '1') then
                    rx_state_next                 <= STATE_READ_DATA;
                    uart_rx_bytes_to_receive_next <= (length-1)/8+1;
                else
                    uart_rx_bytes_to_receive_next <= 0;
                end if;

            when STATE_READ_DATA =>
                if(uart_rx_bytes_to_receive - 1 > 0) then
                    uart_rx_data_buffer_next(uart_rx_bytes_to_receive*8-1 downto (uart_rx_bytes_to_receive-1)*8) <= uart_rx_data;
                    uart_rx_bytes_to_receive_next <= uart_rx_bytes_to_receive - 1;
                    rx_state_next                 <= STATE_WAIT;
                else
                    uart_rx_data_buffer_next(7 downto 0) <= uart_rx_data;
                    uart_rx_bytes_to_receive_next        <= uart_rx_bytes_to_receive - 1;
                    rx_state_next                        <= STATE_SAVE;
                end if;

            when STATE_WAIT =>
                if(uart_rx_new_data = '1') then
                    rx_state_next <= STATE_READ_DATA;
                end if;

            when STATE_SAVE =>
                rng_seed_next <= uart_rx_data_buffer(length-1 downto 0);
                rx_state_next <= STATE_IDLE;
            end case;
    end process rx_state_machine;
end behavioral;