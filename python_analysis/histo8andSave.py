"""
opens a serial port (/dev/ttyUSB1)
geneartes random numbers
sends data to FPGA
deletes data
FPGA loops back
reads data
creates a histogram of received data (x)

"""

import numpy as np
import serial 
import matplotlib.pyplot as plt
import time


file = open('workfile.txt', 'wb+')
num_bins = 256 

#open serial connection
ser = serial.Serial(
    port="/dev/ttyUSB1",
    baudrate= 500000,
    timeout = None)


ser.flushInput()    

#check if open
if not ser.is_open:
    exit(-1)


#prelocate memmory and some variables
read_128bit = []
read_8bit = []

j = 0
i = 0
number_mem = 0

run_loop = True

#------------ read random 128-bit numbers--------------------------------------
while run_loop:
    read_tmp = ser.read(1)          #read from serial port
    file.write(bytes(read_tmp))     #write to file as byte

    read_tmp = ord(read_tmp)        # get unicode <- int

    read_8bit.append(read_tmp)

    number_mem = number_mem << 8    # save as 128-bit number
    number_mem |= read_tmp          # save unicode

    
    
    #save 128 bit  = 16 bytes in one element
    if j == 15:
        j = 0
        read_128bit.append(number_mem)     #add to list
        print(i, ":\t", hex(number_mem), "\t", number_mem)
        i = i+1
        number_mem = 0
        
    else:
        j = j+1

    # break condition
    if i >= 10**5:
        run_loop = False



#close file and serial connection
file.close()
ser.close()

print("number of received bytes: ", len(read_8bit))
print("number of 128bit numbers: ", len(read_128bit))
#print(read)

if len(read_8bit) == 0:
    exit(-1)
#---------------------------display histogram ---------------------------------
mean = np.mean(read_8bit)
std = np.std(read_8bit)
fig, ax = plt.subplots()
# the histogram of the data
n, bins, patches = ax.hist(read_8bit, num_bins)
ax.set_xlabel('Smarts')
ax.set_ylabel('Probability density')
ax.set_title("Histogram of 8-bit numbers: $\mu_o$=%.03lf  $\sigma_o$=%.03lf"%(mean, std))
ax.set_xlim(0,255)

# Tweak spacing to prevent clipping of ylabel
fig.tight_layout()
plt.show()
