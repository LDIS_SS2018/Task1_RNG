"""
opens a serial port (/dev/ttyUSB1)
geneartes random numbers
sends data to FPGA
deletes data
FPGA loops back
reads data
creates a histogram of received data (x)

"""

import numpy as np
import matplotlib.pyplot as plt
import time



#prelocate memmory and some variables

series = []
for i in range(1, 9):
    name = 'serie'+ str(i)+ '.txt'
    file = open(name, 'rb')
    read_bytes = file.read(80)

    list_of_ints = list(map(int, read_bytes))

    if (len(series) == 0):
        series = np.array(list_of_ints[0:30])
    else:
        series = np.vstack([series, list_of_ints[0:30]])
    file.close()


CP = np.corrcoef(series)


for i in range (0, 8):
    for j in range (0, 8):
        print(CP[i,j])
    print(" ")
#---------------------------display plots -------------------------------------

#fig, ax = plt.subplots()
fig = plt.figure()

for i in range (0, 8):

    ax = fig.add_subplot(2,4,i+1)
    plt.subplot(2,4,i+1)
    plt.plot(range(0, len(series[i])), series[i])
    plt.grid()
    #plt.scatter(range(0, len(series[i])), series[i], s= 40)
    ax.set_ylim(0,255)
    ax.set_xlim(0,len(series[0]))
    ax.set_ylabel("n")
    ax.set_xlabel("value")


fig.suptitle("eight series of 30 random bytes after reset", fontsize=16)




# Tweak spacing to prevent clipping of ylabel
fig.tight_layout()
plt.show()
