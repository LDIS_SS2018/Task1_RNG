"""
opens a serial port (/dev/ttyUSB1)
geneartes random numbers
sends data to FPGA
deletes data
FPGA loops back
reads data
creates a histogram of received data (x)

"""

import numpy as np
import serial 
import matplotlib.pyplot as plt
import time


file = open('workfile.txt', 'rb')
num_bins = 256 

list_out = []

for i in range(0, 10**5):
    read_bytes = file.read(16)
    read_list_of_ints = list(map(int, read_bytes))

    out = 0;
    for j in range(0, 16):
        out = out << 8
        out |= read_list_of_ints[j]

    print(i, ":\t", hex(out), "\t", out)
    list_out.append(out)
    
#close file 
file.close()
print("max possible value:\t", 2**128)
print("max read value:\t", np.max(list_out))

#---------------------------display histogram ---------------------------------
fig, ax = plt.subplots()
# the histogram of the data
n, bins, patches = ax.hist(list_out, 256)
ax.set_xlabel('Smarts')
ax.set_ylabel('Probability density')
ax.set_title("Histogram of 128-bit numbers")
#ax.set_xlim(0,255)

# Tweak spacing to prevent clipping of ylabel
fig.tight_layout()
plt.show()
