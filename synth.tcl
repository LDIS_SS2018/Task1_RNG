#------------------------------------------------------------------------------
#
# Synthesis script using Digilent Nexys 4 DDR board
#
# -----------------------------------------------------------------------------
#
create_project -part xc7a100t -force [lindex $argv 0]
#
# -----------------------------------------------------------------------------
#
read_vhdl sevenseg.vhdl
read_vhdl uart_rx.vhd
read_vhdl uart_tx.vhd
read_vhdl rng.vhdl
read_vhdl rbg.vhdl
read_vhdl lfsr.vhdl
read_vhdl garo.vhdl
read_vhdl hw_signal_handler.vhdl
read_vhdl [lindex $argv 0].vhdl
read_xdc  [lindex $argv 0].xdc
#
# -----------------------------------------------------------------------------
#
synth_design -top [lindex $argv 0]
#
# -----------------------------------------------------------------------------
#
opt_design
place_design
route_design
#
# -----------------------------------------------------------------------------
#
write_bitstream -force [lindex $argv 0].bit
#
# -----------------------------------------------------------------------------
