-------------------------------------------------------------------------------
--
-- hw_signal_handler testbench
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--
-------------------------------------------------------------------------------
--
entity hw_signal_handler_tb is
end hw_signal_handler_tb;
--
-------------------------------------------------------------------------------
--
architecture behavior of hw_signal_handler_tb is
    constant WAIT_TICKS   : integer := 5;
    constant ACTIVE_STATE : bit     := '1';

    signal clk           : std_logic;
    signal reset         : std_logic := '0';
    signal signal_in     : std_logic;
    signal db_signal_out : std_logic := '0';
    signal pos_edge_out  : std_logic := '0';
    signal neg_edge_out  : std_logic := '0';

    constant clk_period	: time := 5 ns;

begin

	uut: entity work.hw_signal_handler generic map(WAIT_TICKS, ACTIVE_STATE) 
    port map(clk, reset, signal_in, db_signal_out, pos_edge_out, neg_edge_out);

	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process
	begin
		reset <= '0';

        for i in 0 to 1000 loop
            signal_in <= '0';
            wait for 100 ns;
            signal_in <= '1';
            wait for 100 ns;
        end loop;

		-- Stop simulation
		assert false report"Successfully finished simulation" severity failure;
		--wait;

	end process;

end;
--
-------------------------------------------------------------------------------
