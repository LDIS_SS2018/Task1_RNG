-------------------------------------------------------------------------------
--
-- rng testbench
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--
-------------------------------------------------------------------------------
--
entity rng_tb is
end rng_tb;
--
-------------------------------------------------------------------------------
--
architecture behavior of rng_tb is
    constant length : integer := 2;
    constant ro_polynom : std_logic_vector := "100011101";

	signal clk           : std_logic;
    signal reset         : std_logic := '1';
    signal seed          : std_logic_vector(length-1 downto 0) := (others => '1');
    signal random_number : std_logic_vector(length-1 downto 0) := (others => '0');

    constant clk_period	: time := 5 ns;

begin

	uut: entity work.rng generic map(length, ro_polynom) port map (clk, reset, seed, random_number);

	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process
	begin
		reset <= '0';
        wait for 200 ns;
		-- Stop simulation
		assert false report "Successfully finished simulation" severity failure;
		--wait;
		
	end process;

end;
--
-------------------------------------------------------------------------------
