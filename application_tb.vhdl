-------------------------------------------------------------------------------
--
-- application testbench
--
-------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--
-------------------------------------------------------------------------------
--
entity application_tb is
end application_tb;
--
-------------------------------------------------------------------------------
--
architecture behavior of application_tb is
	signal CLK  : std_logic;
    signal BTNC : std_logic;
    signal BTNR : std_logic;
    signal SW   : std_logic_vector(0 downto 0);

	-- LEDs
	signal LED     : std_logic_vector(15 downto 0) := (others => '0');
	signal LED17_R : std_logic;

	-- 7-Segment
	signal SEVENSEG_CATH : std_logic_vector(7 downto 0);
	signal SEVENSEG_AN   : std_logic_vector(7 downto 0);

	-- UART
	signal UART_RECEIVE  : std_logic;
	signal UART_TRANSMIT : std_logic;

    constant clk_period	: time := 5 ns;

begin

	uut: entity work.application port map (CLK, BTNC, BTNR, SW, LED, LED17_R, SEVENSEG_CATH, SEVENSEG_AN, UART_RECEIVE, UART_TRANSMIT);

	clk_process :process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;

	stim_proc: process
	begin
		btnc <= '0';
		btnr <= '0';
		sw(0) <= '0';
		wait for 200 ns;
		btnr <= '1';
		wait for 10000 ns;
		-- Stop simulation
		assert false report "Successfully finished simulation" severity failure;
		--wait;

	end process;

end;
--
-------------------------------------------------------------------------------
